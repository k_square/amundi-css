import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Parent1Component } from './parent1/parent1.component';
import { Enfant1Component } from './enfant1/enfant1.component';
import { CasPratiqueV1Component } from './cas-pratique-v1/cas-pratique-v1.component';

@NgModule({
  declarations: [
    AppComponent,
    Parent1Component,
    Enfant1Component,
    CasPratiqueV1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
