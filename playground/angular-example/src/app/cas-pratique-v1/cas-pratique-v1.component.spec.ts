import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CasPratiqueV1Component } from './cas-pratique-v1.component';

describe('CasPratiqueV1Component', () => {
  let component: CasPratiqueV1Component;
  let fixture: ComponentFixture<CasPratiqueV1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CasPratiqueV1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CasPratiqueV1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
