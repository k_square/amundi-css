import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cas-pratique-v1',
  templateUrl: './cas-pratique-v1.component.html',
  styleUrls: ['./cas-pratique-v1.component.scss']
})
export class CasPratiqueV1Component implements OnInit {

  tableau = [1, 2, 3, 4, 5];

  constructor() { }

  ngOnInit(): void {
  }

}
